from django.shortcuts import render

# Create your views here.

def home(request):
    print("Home page")
    return render(request, 'home.html',{})

def clients(request):
    print("Client's page")
    return render(request, 'clients/home.html',{} )

def support(request):
    return render(request, 'support/home.html', {})

def director(request):
    return render(request, 'director/home.html')